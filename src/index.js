import SC from 'soundcloud';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import configureStore from './stores/configureStore';
import App from './components/App';
import Home from './components/Home';
import Stream from './components/Stream';
import { CLIENT_ID, REDIRECT_URI } from './constants/auth';

SC.initialize({ client_id: CLIENT_ID, redirect_uri: REDIRECT_URI });

const store = configureStore();

ReactDOM.render(
  <Provider store={ store }>
    <Router>
      <Route component={ App }>
        <Route exact path="/" component={ Stream } />
        <Route path="/home" component={ Home } />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);