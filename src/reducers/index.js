import { combineReducers } from 'redux';
import auth from './auth';
import track from './track';

export default combineReducers({
  auth,
  track,
});