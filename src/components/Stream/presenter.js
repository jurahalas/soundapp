import React, { Component } from 'react';

class Stream extends Component {
  componentDidUpdate() {
    if(!this.refs.audio) return;

    const { activeTrack } = this.props;

    if(activeTrack) {
      this.refs.audio.load();
      this.refs.audio.play();
    }
    else
      this.refs.audio.pause();
  }

  render() {
    const { user, tracks = [], activeTrack, onAuth, onPlay } = this.props;

    return (
      <div>
        <div>
          {
            user ?
              <div>{ user.username }</div> :
              <button onClick={ onAuth } type="button">Login</button>
          }
        </div>
        <br />
        <div>
          {
            tracks.map((track, key) => (
              <div className="track" key={ key }>
                { track.origin && track.origin.title }
                <button type="button" onClick={ () => onPlay(track) }>Play</button>
              </div>
            ))
          }
        </div>
        {
          activeTrack ?
            <audio controls ref="audio" id="audio">
              <source src={ `${activeTrack.origin.stream_url}?client_id=L4uORyiM1NIPHMzZNvMUHBdeFbhHkW9b` } type="audio/ogg" />
            </audio>
            :
            null
        }
      </div>
    );
  }
}

export default Stream;
